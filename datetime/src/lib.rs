use std::thread;
use std::time::Duration;

pub struct DateTime {
    pub time: u64,
}

impl DateTime {
    pub fn increment(&mut self) {
        self.time += 1;
    }
}

pub fn time() -> u64 {
    42
}

pub fn start() {
    thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_time() {
        assert_eq!(time(), 42);
    }
}
