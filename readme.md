# software design

## lib_player

Manages the player. Could have attributes like name, age

Attributes:
* name
* age

## lib_stock

Simulation of a stock. The player can sell/buy stuff. 

Attributes:
* item
  * name
  * price


## lib_map

Manages the environment where the player can move around or build stuff. 

## lib_datetime

Manages date and time. The player can modify the date. Example if he takes a journey (traveling) or if he makes a pause (sleep)

```mermaid
graph TD;
    ui[user interface] --> player[player]
    ui --> | buy/sell/read | stock[stock] 
    player --> date
    maps --> date
    date --> stock
    player --> maps
    date --> ui
```

# pretotype

A first pretotype is done with terminal user interfacde (tui). All libs are prepared. This should give a first idea about the design and architecture of the project.

## arc42

### Introduction and Goals 

Short description of the requirements. Top three quality goals for the architecture which have highest priority for the major stakeholder.

Retrobanker is a incremental, clicking, procedural and competitive banking game. 
There should also be funny elements from the old banking days.

Goals:
* the system should be well balanced
* competitive (no lucky runs for a high score)



### Constraints

Anything that constrains teams in design and implementation decisions or decisions about related process.

### Context and Scope

Show from a business/domain or a technical prespective

### Solution Strategy

Can include technology, top-level decomposition, approaches to achieve top quality goals and relevant organizational decisions.

### Building Block View

Static decomposition of the system, abstractions of source-code, shown as hierarchy of white boxes (containing black boxes), up to the appropriate level of detail.

### Runtime View

Behavior of building blocks as scenarios, covering important use cases or features, interactions at critical external interfaces, operation and administration plus error and exception behavior.

### Deployment View

Technical infrastructure. Mapping of software building blocks to infrastructure elements.

### Crosscutting Concepts

Overall, principal regulations and solution approaches relevant in multiple parts (→ cross-cutting) of the system. Concepts are often related to multiple building blocks. Include different topics like domain models, architecture patterns and -styles, rules for using specific technology and implementation rules.

### Architectural Decisions

Important, expensive, critical, large scale or risky architecture decisions including rationales.

* user interface can change at any time
* the interface of the libs should be small
* the implementation of the libs should be deep

### Quality Requirements

Quality requirements as scenarios, with quality tree to provide high-level overview. The most important quality goals should have been described in section 1.2. (quality goals).

### Risk and Technical Debt

Known technical risks or technical debt. What potential problems exist within or around the system? What does the development team feel miserable about?

### Glossary

Domain and technical terms that stakeholders use when discussing the system.

### main goal


### use cases



